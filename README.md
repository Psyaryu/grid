ReadMe
=========

#### How to build:

_Prerequisists Programs:_
 - Git (used: version 1.8.5.2 (Apple Git-48), other versions should be compatible)
 - XCode 6 Beta 2 IDE

_Retrieving source in terminal:_
 - git clone <REPO URL> <FOLDER NAME>

_Running the project:_
 - Open and run .xcodeproject


#### How to use:

???

_Setup_

        [self codeHere];
    